import React from "react";
import { Button, Col, Form } from "react-bootstrap";

function Updatecourse(props) {
  return (
    <div>
    <h1>Update Course</h1>
      <Form>
        <Form.Row>
          <Form.Group controlId="formGridAddress1">
            <Form.Label>Course ID</Form.Label>
            <Form.Control placeholder="Course ID" />
          </Form.Group>
          <Form.Group as={Col} controlId="formGridEmail">
            <Form.Label>Course Name</Form.Label>
            <Form.Control type="text" placeholder="Course Name" />
          </Form.Group>

          <Form.Group as={Col} controlId="formGridPassword">
            <Form.Label>Course Price</Form.Label>
            <Form.Control type="password" placeholder="Price" />
          </Form.Group>
        </Form.Row>

        <Form.Group controlId="formGridAddress1">
          <Form.Label>Course Description</Form.Label>
          <Form.Control placeholder="Description" />
        </Form.Group>
        <Form.Group controlId="formGridAddress1">
          <Form.Label>Start Date</Form.Label>
          <Form.Control type="date" placeholder="Start Date" />
        </Form.Group>
        <Form.Group controlId="formGridAddress1">
          <Form.Label>End Date</Form.Label>
          <Form.Control type="date" placeholder="End Date" />
        </Form.Group>

        <Button variant="primary" type="submit">
          Update Course
        </Button>
      </Form>
    </div>
  );
}

export default Updatecourse;
