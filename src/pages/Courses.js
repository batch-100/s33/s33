import React, { Fragment, useContext } from 'react';
import { Button, Table } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Course from '../components/Course';
//import userContext
import courseData from '../data/courses';
import UserContext from '../UserContext';

export default function Courses(){
	//use the UserContext and desctructure it to access to user state defined
	const {user} = useContext(UserContext);
	//create multiple course component corresponding to the content of courseData
	const courses = courseData.map(course => {
		if(course.onOffer){
			
			return(
				<Course key={course.id} course={course}/>
			);
		} else {
			return null;
		}
	});
 

	//table rows to be rendered in abootstrap table when an admin is logged in
	const coursesRows = courseData.map(course => {
		//console.log(course.onOffer)
		
		
		return (
			<tr key={course.id}>
				<td> {course.id} </td>
				<td> {course.name} </td>
				<td> {course.description} </td>
				<td> Php {course.price}</td>
				<td> {course.onOffer ? 'open' : 'closed'}</td>
				<td> {course.start_date}</td>
				<td> {course.end_date}</td>
				<td>
					<Link to="/updatecourse" className="btn btn-warning">Update</Link>
					<Button variant="danger" key={course.id} onClick={()=>{course.onOffer=false}}>Disable</Button> 
				</td>
			</tr>
			
		);
	})

	return (
		user.isAdmin === true ? 
		<Fragment>
			<h1>Courses Dashboard</h1>
			<Table>
				<thead>
				<tr>
					<th>ID</th>
					<th>Name</th>
					<th>Description</th>
					<th>Price</th>
					<th>Status</th>
					<th>Start Date</th>
					<th>End Date</th>
					<th>Actions Taken</th>
				</tr>
				</thead>
				<tbody>
					{coursesRows}
				</tbody>
			</Table>
		</Fragment>
		:
		<Fragment>
			{courses}
		</Fragment>
	)

}